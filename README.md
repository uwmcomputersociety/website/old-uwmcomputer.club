This repo is the full code of the website.

The previous Bootstrap readme has been moved to BOOTSTRAP.md.

# WARNING
- Do not put anything that shouldn't be publicly visible in here! Anyone can see this repo. Additionally, as it is configured, directories such as `.git` are visible from the webserver.