$( document ).ready(function() {
    $("[rel='tooltip']").tooltip();    

    $('.thumbnail').hover(
        function(){
            $(this).find('.caption').slideDown(250);
        },
        function(){
            $(this).find('.caption').slideUp(250);
        }
    );
    $('.thumbnail').click(
        function(){
            var $caption = $(this).find('.caption');

            if ($caption.css('display') === "none")
                $caption.slideDown(250);
            else $caption.slideUp(250);
        }
    );
});